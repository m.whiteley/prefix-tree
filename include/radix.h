#include <cstdint>

#define NULL = UINT64_MAX

class RadixTree
{
public:
    /**
     * Constructor takes a pointer to the memory to store values in and its capacity
     */
    RadixTree( uint8_t* mem, uint64_t size )
    : memory( mem )
    , capacity( size )
    {
        root = RadixNode();
    };

    /**
     * Inserts or updates the value at the given key
     * returns true if the key already existed in the tree else false
     */
    bool insert( uint64_t key, uint64_t value );

    /**
     * Find the value at the given key
     * returns the value if found else UINT64_MAX
     */
    uint64_t find( uint64_t key );

    /**
     * Removes the given key and its value from the tree
     * returns true if the key was removed, false if it wasn't found
     */
    bool remove( uint64_t key );

private:
    /**
     * Finds the next node to go to from the given node
     * Returns a pointer to the next node
     */
    uint64_t* traverse( RadixNode node, uint64_t key );

    const uint8_t* memory;
    const uint64_t capacity;

    RadixNode root;
};

class RadixNode
{
    bool leaf;

};